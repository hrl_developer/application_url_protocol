import re
import sys

def parse_url(url):
    params = url.split('?')[1]
    return dict(re.findall(r'([^=\&]*)=([^\&]*)', params))

print('HRLD application hrldx protocol')
for key in parse_url(sys.argv[1]).keys():
    print('%s: %s' % (key, parse_url(sys.argv[1])[key]))
    
input()
